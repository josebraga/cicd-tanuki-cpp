# CI/CD Tanuki C++

C++ in CI/CD with Docker Builder Image optimizations for Pipeline Efficiency.

Tip: View the project history and merge request to follow the steps to be done. 

**Inspect the pipelines and job output to [learn more](https://gitlab.com/gitlab-de/cicd-tanuki-cpp/-/jobs/1662516494).**

## Resources

* [GitLab documentation: Pipeline Efficiency](https://docs.gitlab.com/ee/ci/pipelines/pipeline_efficiency.html)
* [GitLab Learn](https://about.gitlab.com/learn/)
